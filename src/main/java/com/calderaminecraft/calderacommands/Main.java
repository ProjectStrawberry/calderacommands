package com.calderaminecraft.calderacommands;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Main extends JavaPlugin {
    static Main plugin;
    static FileConfiguration blocks;
    static File blockData;
    static List<String> isSetting = new ArrayList<>();
    static HashMap<String, Boolean> isEditing = new HashMap<>();
    private Injector<PlayerInteractEvent> injector = new Injector<PlayerInteractEvent>(PlayerInteractEvent.class);

    @Override
    public void onEnable() {
        plugin = this;
        plugin.getDataFolder().mkdirs();

        blockData = new File(plugin.getDataFolder(), "blocks.yml");
        if (!blockData.exists()) {
            try {
                blockData.createNewFile();
            } catch (IOException e) {
                plugin.getLogger().severe("Error creating data file: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                plugin.getLogger().warning("CalderaCommands failed to load.");
                return;
            }
        }
        blocks = YamlConfiguration.loadConfiguration(blockData);

        this.getCommand("calderacommands").setExecutor(new ComandCC());
        getServer().getPluginManager().registerEvents(new EventListeners(), this);

        plugin.getLogger().info("CalderaCommands loaded.");
    }

    @Override
    public void onDisable() {
        injector.close();
    }
}
