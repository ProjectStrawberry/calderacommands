package com.calderaminecraft.calderacommands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ComandCC implements CommandExecutor {
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!sender.hasPermission("calderacommands.use")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
            return true;
        }
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
            return true;
        }
        Player player = (Player) sender;
        if (args.length < 1) {
            player.sendMessage(ChatColor.GOLD + "Usage: /cc set OR /cc unset");
            return true;
        }

        if (args[0].equals("set")) {
            Main.isEditing.put(player.getUniqueId().toString(), true);
            player.sendMessage(ChatColor.GOLD + "Click a block to enable the ServerSigns compatibility flag.");
            return true;
        }

        if (args[0].equals("unset")) {
            Main.isEditing.put(player.getUniqueId().toString(), false);
            player.sendMessage(ChatColor.GOLD + "Click a block to remove the ServerSigns compatibility flag.");
            return true;
        }

        player.sendMessage(ChatColor.GOLD + "Usage: /cc set OR /cc unset");
        return true;
    }
}
