package com.calderaminecraft.calderacommands;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class EventListeners implements Listener {
    @EventHandler (priority = EventPriority.HIGHEST)
    public void PlayerInteractEvent(PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        final Block block = event.getClickedBlock();

        if (block == null) return;
        Location loc = block.getLocation();
        final Material material = block.getType();
        BlockFace blockface = event.getBlockFace();
        String blockString = loc.getWorld().getName() + ":" + loc.getBlockX() + ":" + loc.getBlockY() + ":" + loc.getBlockZ();

        if (Main.isEditing.containsKey(player.getUniqueId().toString())) {
            if (block.getType().equals(Material.SIGN)) {
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You can't set the ServerSigns compatibility flag on signs.");
                event.setCancelled(true);
                return;
            }

            if (Main.isEditing.get(player.getUniqueId().toString())) {
                if (Main.blocks.get("locations") == null) {
                    Main.blocks.set("locations", new ArrayList<>());
                }

                if (Main.blocks.getStringList("locations").contains(blockString)) {
                    player.sendMessage(ChatColor.RED + "Error: " + "The ServerSigns compatibility flag is already set on this block.");
                    event.setCancelled(true);
                    Main.isEditing.remove(player.getUniqueId().toString());
                    return;
                }
                List<String> blocks = Main.blocks.getStringList("locations");
                blocks.add(blockString);
                Main.blocks.set("locations", blocks);
                try {
                    Main.blocks.save(Main.blockData);
                } catch (IOException e) {
                    Main.plugin.getLogger().severe("Error saving data file: " + e.getClass().getName() + ": " + e.getMessage());
                    e.printStackTrace();
                    player.sendMessage(ChatColor.DARK_RED + "Error setting flag.");
                    event.setCancelled(true);
                    return;
                }
                event.setCancelled(true);
                Main.isEditing.remove(player.getUniqueId().toString());
                player.sendMessage(ChatColor.GOLD + "The ServerSigns compatibility flag has been set on this block.");
                return;
            }

            if (!Main.isEditing.get(player.getUniqueId().toString())) {
                if (Main.blocks.get("locations") == null) {
                    player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This block does not have the ServerSigns compatibility flag set.");
                    event.setCancelled(true);
                    return;
                }

                if (!Main.blocks.getStringList("locations").contains(blockString)) {
                    player.sendMessage(ChatColor.RED + "Error: " + "This block does not have the ServerSigns compatibility flag set.");
                    event.setCancelled(true);
                    Main.isEditing.remove(player.getUniqueId().toString());
                    return;
                }
                List<String> blocks = Main.blocks.getStringList("locations");
                blocks.remove(blockString);
                Main.blocks.set("locations", blocks);
                try {
                    Main.blocks.save(Main.blockData);
                } catch (IOException e) {
                    Main.plugin.getLogger().severe("Error saving data file: " + e.getClass().getName() + ": " + e.getMessage());
                    e.printStackTrace();
                    player.sendMessage(ChatColor.DARK_RED + "Error removing flag.");
                    event.setCancelled(true);
                    return;
                }
                event.setCancelled(true);
                Main.isEditing.remove(player.getUniqueId().toString());
                player.sendMessage(ChatColor.GOLD + "The ServerSigns compatibility flag has been removed from that block.");
                return;
            }
        }
    }
}