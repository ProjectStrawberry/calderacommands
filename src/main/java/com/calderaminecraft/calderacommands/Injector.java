/*
* based on https://gist.githubusercontent.com/aadnk/5563794/raw/b894b72edadb641a38b498b64356841bdc638145/CancellationDetector.java
 */

package com.calderaminecraft.calderacommands;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.block.Skull;
import org.bukkit.block.data.BlockData;
import org.bukkit.event.Event;
import org.bukkit.event.EventException;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.plugin.IllegalPluginAccessException;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredListener;

import com.google.common.collect.Lists;

public class Injector<TEvent extends Event> {
    interface InjectedListener<TEvent extends Event> {
        public void onCancelled(Plugin plugin, TEvent event);
    }

    private final Class<TEvent> eventClazz;
    private final List<InjectedListener<TEvent>> listeners = Lists.newArrayList();

    // For reverting the detector
    private EnumMap<EventPriority, ArrayList<RegisteredListener>> backup;

    public Injector(Class<TEvent> eventClazz) {
        this.eventClazz = eventClazz;
        injectProxy();
    }

    @SuppressWarnings("unchecked")
    private EnumMap<EventPriority, ArrayList<RegisteredListener>> getSlots(HandlerList list) {
        try {
            return (EnumMap<EventPriority, ArrayList<RegisteredListener>>) getSlotsField(list).get(list);
        } catch (Exception e) {
            throw new RuntimeException("Unable to retrieve slots.", e);
        }
    }

    private Field getSlotsField(HandlerList list) {
        if (list == null)
            throw new IllegalStateException("Detected a NULL handler list.");

        try {
            Field slotField = list.getClass().getDeclaredField("handlerslots");

            // Get our slot map
            slotField.setAccessible(true);
            return slotField;
        } catch (Exception e) {
            throw new IllegalStateException("Unable to intercept 'handlerslot' in " + list.getClass(), e);
        }
    }

    private void injectProxy() {
        HandlerList list = getHandlerList(eventClazz);
        EnumMap<EventPriority, ArrayList<RegisteredListener>> slots = getSlots(list);

        // Keep a copy of this map
        backup = slots.clone();

        synchronized (list) {
            for (EventPriority p : slots.keySet().toArray(new EventPriority[0])) {
                if (p.equals(EventPriority.MONITOR)) continue;
                final EventPriority priority = p;
                final ArrayList<RegisteredListener> proxyList = new ArrayList<RegisteredListener>() {
                    private static final long serialVersionUID = 7869505892922082581L;

                    @Override
                    public boolean add(RegisteredListener e) {
                        super.add(injectRegisteredListener(e));
                        return backup.get(priority).add(e);
                    }

                    @Override
                    public boolean remove(Object listener) {
                        // Remove this listener
                        for (Iterator<RegisteredListener> it = iterator(); it.hasNext(); ) {
                            DelegatedRegisteredListener delegated = (DelegatedRegisteredListener) it.next();
                            if (delegated.delegate == listener) {
                                it.remove();
                                break;
                            }
                        }
                        return backup.get(priority).remove(listener);
                    }
                };
                slots.put(priority, proxyList);

                for (RegisteredListener listener : backup.get(priority)) {
                    proxyList.add(listener);
                }
            }
        }
    }

    // The core of our magic
    private RegisteredListener injectRegisteredListener(final RegisteredListener listener) {
        return new DelegatedRegisteredListener(listener) {
            @SuppressWarnings("unchecked")
            @Override
            public void callEvent(Event event) throws EventException {
                if (event instanceof PlayerInteractEvent && getPlugin().getName().equals("ServerSigns")) {
                    PlayerInteractEvent playerEvent = (PlayerInteractEvent) event;
                    Block block = playerEvent.getClickedBlock();
                    if (block == null || playerEvent.getHand() == null || !playerEvent.getHand().equals(EquipmentSlot.HAND)) {
                        listener.callEvent(event);
                        return;
                    }
                    Material material = block.getType();
                    Location loc = block.getLocation();
                    String blockString = loc.getWorld().getName() + ":" + loc.getBlockX() + ":" + loc.getBlockY() + ":" + loc.getBlockZ();
                    if (!block.getType().equals(Material.SIGN) && Main.blocks.get("locations") != null && Main.blocks.getStringList("locations").contains(blockString)) {
                        Skull head;
                        if (block.getState() instanceof Skull) {
                            head = (Skull) block.getState();
                            BlockData oldData = block.getBlockData().clone();

                            block.setType(Material.SIGN);
                            Skull finalHead = head;

                            listener.callEvent(event);

                            block.setType(material);
                            block.setBlockData(oldData);
                            if (finalHead != null) {
                                Skull targetBlock = (Skull) block.getState();
                                if (finalHead.hasOwner()) {
                                    OfflinePlayer newOwner = finalHead.getOwningPlayer();
                                    targetBlock.setOwningPlayer(newOwner);
                                    targetBlock.update();
                                }
                            }
                        } else {
                            BlockData oldData = block.getBlockData().clone();

                            block.setType(Material.SIGN);

                            listener.callEvent(event);

                            block.setType(material);
                            block.setBlockData(oldData);
                        }
                    } else {
                        listener.callEvent(event);
                    }
                } else {
                    listener.callEvent(event);
                }
            }
        };
    }

    public void close() {
        if (backup != null) {
            try {
                HandlerList list = getHandlerList(eventClazz);
                getSlotsField(list).set(list, backup);

                Field handlers = list.getClass().getDeclaredField("handlers");
                handlers.setAccessible(true);
                handlers.set(list, null);

            } catch (Exception e) {
                throw new RuntimeException("Unable to clean up handler list.", e);
            }

            backup = null;
        }
    }

    /**
     * Retrieve the handler list associated with the given class.
     *
     * @param clazz  - given event class.
     * @return Associated handler list.
     */
    private static HandlerList getHandlerList(Class<? extends Event> clazz) {
        // Class must have Event as its superclass
        while (clazz.getSuperclass() != null && Event.class.isAssignableFrom(clazz.getSuperclass())) {
            try {
                Method method = clazz.getDeclaredMethod("getHandlerList");
                method.setAccessible(true);
                return (HandlerList) method.invoke(null);
            } catch (NoSuchMethodException e) {
                // Keep on searching
                clazz = clazz.getSuperclass().asSubclass(Event.class);
            } catch (Exception e) {
                throw new IllegalPluginAccessException(e.getMessage());
            }
        }
        throw new IllegalPluginAccessException("Unable to find handler list for event "
                + clazz.getName());
    }

    /**
     * Represents a registered listener that delegates to a given listener.
     * @author Kristian
     */
    private static class DelegatedRegisteredListener extends RegisteredListener {
        private final RegisteredListener delegate;

        public DelegatedRegisteredListener(RegisteredListener delegate) {
            // These values will be ignored however'
            super(delegate.getListener(), null, delegate.getPriority(), delegate.getPlugin(), false);
            this.delegate = delegate;
        }

        public void callEvent(Event event) throws EventException {
            delegate.callEvent(event);
        }

        public Listener getListener() {
            return delegate.getListener();
        }

        public Plugin getPlugin() {
            return delegate.getPlugin();
        }

        public EventPriority getPriority() {
            return delegate.getPriority();
        }

        public boolean isIgnoringCancelled() {
            return delegate.isIgnoringCancelled();
        }
    }
}